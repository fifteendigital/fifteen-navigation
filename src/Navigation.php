<?php 

namespace Fifteen\Navigation;

class Navigation
{
    protected $repo;
    protected $navs = [];

    public function setNav($name, $nav)
    {
        $this->navs[$name] = $nav;
    }

    public function removeNav($name)
    {
        unset($this->navs[$name]);
    }

    public function setActiveUrl($url)
    {
        $navs = $this->get();
        $nav = array_shift($navs);
        $nav->setActiveUrl($url);
    }

    public function get($name = null)
    {
        if ($name == null) {                    // return all settings
            return $this->navs;
        } else {                                // return setting with key provided
            if (isset($this->navs[$name])) {
                return $this->navs[$name];
            }
        }
    }

    function __get($name) 
    {
        return $this->get($name);
    }
    function __call($function, $parameters) 
    {
        return $this->get($function);
    }
}