<?php

namespace Fifteen\Navigation\Support;

use \Illuminate\Support\Facades\Request;

abstract class AbstractNavigationController extends AbstractTree implements NavigationControllerInterface {

    protected static $linkClass;
    protected static $items;
    protected $activeUrl;

    public function __construct()
    {
        $this->activeUrl = Request::url();
    }

    public function updateTree()
    {
        $this->nodes = [];

        foreach (static::$items as $name => $data)
        {
            $node = new static::$linkClass($this, $name, $data);
            if ($node->isVisible())
            {
                $this->nodes[] = $node;
            }
        }
    }

    public static function create()
    {
        return new static;
    }

    public function getActiveUrl()
    {
        return $this->activeUrl;
    }

    public function setActiveUrl($url)
    {
        return $this->activeUrl = $url;
    }
}
