<?php 

namespace Fifteen\Navigation\Support;

abstract class AbstractTreeNode {

    public $parent;
    public $children;
    public $level = 0;

    public function isRoot()
    {
        return ( ! empty($this->parent));
    }

    public function hasChildren()
    {
        return ( ! empty($this->children));
    }

}