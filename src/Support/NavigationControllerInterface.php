<?php 

namespace Fifteen\Navigation\Support;

interface NavigationControllerInterface {

    public function getActiveUrl();

}