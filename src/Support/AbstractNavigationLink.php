<?php

namespace Fifteen\Navigation\Support;

use Illuminate\Support\Facades\Auth;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

abstract class AbstractNavigationLink extends AbstractTreeNode implements NavigationLinkInterface {

    protected $authenticator;
    public $tree;
    public $url;
    public $route;
    public $title;
    public $icon;
    public $permissions;
    public $class;

    public function __construct($tree, $name, $data, $parent = null, $level = 0)
    {
        foreach (['url', 'route', 'title', 'lang', 'icon', 'permissions', 'class'] as $item) {
            switch ($item) {
                case 'permissions':
                    if ( ! empty($data[$item])) {
                        if (is_array($data[$item])) {
                            $this->$item = $data[$item];
                        } else {
                            $this->$item = explode('|', $data[$item]);
                        }
                    }
                    break;
                default:
                    if ( ! empty($data[$item])) {
                        $this->$item = $data[$item];
                    }
                    break;
            }
        }
        if (empty($this->title))
        {
            if ( ! empty($this->lang)) {
                $this->title = \Alang::get($this->lang);
            } else {
                $this->title = $name;
            }
        }
        $this->children = [];
        if (! empty($data['items'])) {
            foreach ($data['items'] as $name => $data) {
                $node = new static($tree, $name, $data, $this, $level + 1);
                if ($node->isVisible()) {
                    $this->children[] = $node;
                }
            }
        }
        $this->tree = $tree;
        $this->parent = $parent;
        $this->level = $level;
    }

    public function getUrl()
    {
        if ($this->url) {
            return $this->url;
        } elseif ($this->route) {
            if (is_array($this->route)) {
                $params = $this->route;
                $name = array_shift($params);
                return route($name, $params);
            }
            return route($this->route);
        }
    }

    public function isActive()
    {
        $url = $this->tree->getActiveUrl();
        if ($url == $this->getUrl()) {
            return true;
        }
        return $this->hasActiveChildren();
    }

    public function hasActiveChildren()
    {
        foreach ($this->children as $item) {
            if ($item->isActive()) {
                return true;
            }
        }
        return false;
    }

    public function isVisible()
    {
        if ( ! empty($this->permissions)) {
            if (Sentinel::check()) {
                foreach ($this->permissions as $permission) {
                    if (Sentinel::hasAccess($permission)) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }
}
