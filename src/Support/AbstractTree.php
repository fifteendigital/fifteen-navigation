<?php

namespace Fifteen\Navigation\Support;

abstract class AbstractTree {

    protected $nodes = [];

    public function getNodes()
    {
        return $this->nodes;
    }

    public function getRootNodes()
    {
        return $this->getNodes();
    }

    public function getNodeByAddress($address_string)
    {
        $address = explode('.', $address_string);
        $nodes = $this->getRootNodes();
        while (count($address) > 0) {
            $title = array_shift($address);
            $node = $this->getNodeByTitle($title, $nodes);
            if (empty($node)) {
                return;
            }
            $nodes = $node->children;
        }
        return $node;
    }

    public function getNodeByTitle($title, $nodes)
    {
        foreach ($nodes as $node) {
            if (str_slug($title) == str_slug($node->title)) {
                return $node;
            }
        }
    }
}
