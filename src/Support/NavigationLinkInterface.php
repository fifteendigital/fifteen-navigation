<?php 

namespace Fifteen\Navigation\Support;

interface NavigationLinkInterface {

    public function getUrl();

    public function isActive();

    public function hasActiveChildren();

}