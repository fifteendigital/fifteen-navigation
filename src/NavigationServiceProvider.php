<?php

namespace Fifteen\Navigation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Lang;

class NavigationServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton('navigation', function($app)
        {
            return \App::make('\Fifteen\Navigation\Navigation');
        });

    }

    public function boot()
    {
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['fifteen.navigation'];
    }

}
