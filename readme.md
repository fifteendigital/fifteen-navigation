# Navigation

A package to help with managing navigation in your app.

###Installation

Add Navigation to your composer.json file:

```
    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/navigation": "1.1.*"
    },
```

As the repository isn't on Packagist, you also need to include it in the repositories list:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-navigation.git"
        }
    ]
```

Run composer update:

```sh
> composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        Fifteen\Navigation\NavigationServiceProvider::class,
    ];
```

Add Navigation to the aliases:

```php
    'Navigation' => Fifteen\Navigation\Facades\Navigation::class,
```

### Usage

Create a navigation controller:

```php
<?php

namespace App\Navigation;

use Fifteen\Navigation\Support\AbstractNavigationController;
use Fifteen\Navigation\Support\DefaultNavigationLink;

class MainNavigationController extends AbstractNavigationController
{

    public static $linkClass = DefaultNavigationLink::class;

    public static $items = [
        'Home' => [
            'route' => 'home',
            'icon' => 'fa-home',
            'lang' => 'general.home',
        ],

        'Admin' => [
            'permissions' => 'menu.admin',
            'icon' => 'fa-cog',
            'items' => [
                'Users' => [
                    'route' => 'users.index',
                    'lang' => 'general.users',
                ],
                'User groups' => [
                    'route' => 'groups.index',
                    'lang' => 'general.user_groups',
                ],
                'Database backups' => [
                    'route' => 'database-backups.index',
                    'lang' => 'general.database_backups',
                ],
            ],
        ]
    ];
}
```

Reference it in your controller file's (or base controller file's) constructor:

```php
\Navigation::setNav('main', MainNavigationController::create());
```

Create a view which iterates through the navigation items recursively:
```html
<li class="
    @if ($item->isActive()) active @endif
    @if ($i == 0 and $item->level == 0) start @endif
    ">
    @if ($item->hasChildren())
        <a href="javascript:;">
            @if ($item->icon)
                <i class="fa {{ $item->icon }}"></i>
            @endif
            <span class="title">
                {{ $item->title }}
            </span>
            @if ($item->isActive())
                <span class="selected"></span>
                <span class="arrow open"></span>
            @else
                <span class="arrow"></span>
            @endif

        </a>
        <ul class="sub-menu">
            @foreach ($item->children as $i => $item)
                @include('layouts.partials.nav')
            @endforeach
        </ul>
    @else
        <a href="{{ $item->getUrl() }}">
            @if ($item->icon)
                <i class="fa {{ $item->icon }}"></i>
            @endif
            <span class="title">
                {{ $item->title }}
            </span>
            @if ($item->isActive())
                <span class="selected"></span>
            @endif
        </a>
    @endif
</li>
 ```

Reference this in your template view:

 ```php
    @foreach ($main_nav->getNodes() as $i => $item)
        @include('layouts.partials.nav')
    @endforeach
```

Pass the main_nav variable using a view composer:

```php
<?php

namespace App\Http\ViewComposers;

use Navigation;

class SidebarComposer {

    protected $nav;

    public function __construct()
    {
        $this->nav = Navigation::get('main');
        $this->setDynamicPages();
    }

    public function setDynamicPages()
    {

    }

    public function compose($view)
    {
        $nav = $this->nav;
        $data = $view->getData();
        if (isset($data['activeUrl']))
        {
            $nav->setActiveUrl($data['activeUrl']);
        }
        $view->with('main_nav', $nav);
    }

}
```

### Set active url

If you wish to manually override the active url (for example if you are viewing a sub-page), you can use setActiveUrl:
```php
Navigation::get('main')->setActiveUrl(route('groups.index'));
```

### Add dynamic links
You can add dynamic links to the tree in the following way:

Declare a method in your NavigationController which finds the node you wish to append to, then add a new link instance to the node's children[] array.
```php
public function addProcesses()
{
    // find the node (use '.' to access multiple levels)
    $node = $this->getNodeByAddress('asset_provisioning.processes');

    // get data from the database
    $this->processesRepo = app(ProcessesRepository::class);
    $processes = $this->processesRepo->all();

    // loop through items
    foreach ($processes as $process) {
        $data = ['route' => ['processes.check', $process->id];

        // append to the children[] array
        $node->children[] = new DefaultNavigationLink($this, $process->name, $data]);
    }
}
```

Then call this method in a method which overrides the NavigationController's updateTree() method.
```php
public function updateTree()
{
    parent::updateTree();
    $this->addProcesses();
}
```
